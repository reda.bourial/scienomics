const express = require('express');
const { getSchedule } = require("./src/index");
// Constants
const public = `${__dirname}/public`;
const port = 3000;

// Setup app
const app = express()
app.use(express.json());
app.use(express.static(public));

app.all('/endpoint', (req, res, next) => {
  const { product, machines } = req.body;
  const answer = getSchedule(product, machines);
  res.send(JSON.stringify(answer));
})

app.listen(port);
