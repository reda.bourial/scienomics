const { DependencyTree } = require("./dependency_tree");
const { MachineMap } = require("./machine_map");

/**
 * Computes the schedule to make the product.
 * @param {} product to make.
 * @param {*} machines available.
 * @returns {Array<Event>} steps to make the product with the given machines.
 */
function getSchedule(product, machines) {
    if (!product) {
        return [];
    }
    try {
        return new DependencyTree(product, new MachineMap(machines)).getSchedule();
    } catch (e) {
        console.error(e)
        return [{ "event": "error", machines: [e.toString()] }];
    }
}

module.exports = { getSchedule }