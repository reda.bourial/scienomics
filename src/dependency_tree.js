const { flatten } = require("./flatten");

/**
 * DependencyTree is the class responsible for managing dependencies and computing the schedule
 */
class DependencyTree {

    /**
     * @param {string} product to make
     * @param {MachineMap} machines available
     */
    constructor(product, machines) {
        this.machine = machines.getMachine(product);
        this.deps = this.machine.need.map(p => new DependencyTree(p, machines));
        this.started = false;
    }

    /**
     * @returns {Integer} tree's depth 
     */
    getDepth() {
        let depth = 0;
        if (this.deps.length) {
            const depths = this.deps.map(d => d.getDepth());
            depth = Math.max(...depths) + 1;
        }
        return depth;
    }

    /**
     * Start machines, to get started a machines need to have all its dependencies available.
     * It chooses machines that are single leaves
     * @returns {Array<String>} started machines
     */
    startMachines() {
        if (this.getDepth() == 0 && this.started == false) {
            this.started = true;
            return [this.machine.name];
        }
        return flatten(this.deps.map(d => d.startMachines()));
    }

    /**
     * Stops machines of which the product are needed.
     * @param {Integer} desiredDepth depth of dependencies that are needed. 
     * @returns {Array<String>} machines to stop.
     */
    stopMachines(desiredDepth) {
        if (desiredDepth == 1) {
            const endedMachines = this.deps.filter(d => d.started).map(d => d.machine.name);
            this.deps = this.deps.filter(d => !d.started);
            return endedMachines;
        }
        return flatten(this.deps.map(d => d.stopMachines(desiredDepth - 1)));
    }

    /**
     * Computes the schedule for making the product.
     * @returns {Array<Event>} events in the right order to make the product. 
     */
    getSchedule() {
        const events = [];
        while (this.getDepth() > 0) {
            const startMachines = this.startMachines();
            events.push({ event: "start", machines: startMachines });
            const stopMachines = this.stopMachines(this.getDepth());
            events.push({ event: "stop", machines: stopMachines });
        }
        events.push(...[
            {
                event: "start",
                machines: [this.machine.name],
            },
            {
                event: "stop",
                machines: [this.machine.name],
            },
        ])
        return events;
    }
}

module.exports = { DependencyTree }