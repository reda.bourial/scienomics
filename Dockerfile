
FROM node:latest
ENV NODE_ENV=production
WORKDIR /app

CMD ["node", "index.js"]