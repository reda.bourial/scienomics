const _ = require('lodash');

/**
 * Flattens a nested array into a simple one
 * @param {Array<Array|String>} arr nest array to flatten 
 * @returns {Array<String>} flattened array
 */
function flatten(arr) {
    return _.uniq(_.flatten(arr));
}

module.exports = {flatten};