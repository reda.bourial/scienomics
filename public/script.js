
// Webpage state
const machines = [
    {
        name: "Forrest",
        need: [],
        produce: "tree",
    },
    {
        name: "Sawmill",
        need: ["tree"],
        produce: "wood",
    },
    {
        name: "Papermill",
        need: ["wood", "water"],
        produce: "paper",
    },
    {
        name: "River",
        need: [],
        produce: "water",
    },
    {
        name: "Aquarium",
        need: ["water","octopus"],
        produce: "ink",
    },
    {
        name: "Fisherman",
        need: ["money"],
        produce: "octopus",
    },
    {
        name: "Bank",
        need: [],
        produce: "money",
    },
    {
        name: "Printer",
        need: ["ink","paper"],
        produce: "book",
    },
]

/**
 * machineToElement converts a machine object to an html element
 * @param {machine} machine to convert to an element 
 * @param {Integer} index machine's index 
 * @returns {Element} representing the machine
 */
function machineToElement({ name, need, produce }, index) {
    const title = document.createElement("h5");
    title.innerText = name;
    const needs = document.createElement("h6");
    needs.innerText = `needs : ${need.length ? need.join(" and ") : "nothing"}`;
    const produces = document.createElement("h6");
    produces.innerText = `produces : ${produce}`;
    const deleteBtn = document.createElement("button");
    deleteBtn.onclick = () => {
        machines.splice(index, 1);
        refresh()
    }
    deleteBtn.classList = "btn btn-danger";
    deleteBtn.innerText = "Delete"
    const container = document.createElement("div")
    container.classList = "machine-card";
    [title, needs, produces, deleteBtn].map(e => container.append(e));
    const parent = document.createElement("div");
    parent.classList = "col-3";
    parent.append(container)
    return parent;
}

/**
 * Refreshes the machines element
 */
function refreshMachines() {
    const container = document.getElementById("machines-container");
    container.innerHTML = '';
    machines.map(machineToElement).map((e) => container.append(e));
}

/**
 * Refreshes the Products element
 */
function refreshProducts() {
    const container = document.getElementById("select");
    container.innerHTML = '';
    const products = ["", ..._.uniq(machines.map(m => m.produce))];
    products.map(p => {
        const option = document.createElement("option");
        option.innerText = p;
        return option;
    }).map(o => container.append(o))
}

/**
 * Queries the api to get the step to make the product
 * @param {string} product to make
 * @returns {Array<Event>} events in the right order
 */
function queryApi(product) {
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", "/endpoint", false); // false for synchronous request
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.send(JSON.stringify({ product, machines }));
    return JSON.parse(xmlHttp.responseText);
}

/**
 * Refreshes the answer shown to the user
 * @param {string} product selected by the user 
 * @returns {null}
 */
function refreshAnswer(product) {
    const container = document.getElementById("answer");
    container.innerText = '';
    const response = queryApi(product);
    
    const steps = response.map(({ event, machines }) => {
        const e = document.createElement("li")
        e.innerText = `${event} : ${machines.join(", ")}.`
        return e
    })
    const ol = document.createElement("ol")
    ol.append(...steps)
    container.append(ol)
}

/**
 * Refreshes the entire webpage
 */
function refresh() {
    refreshMachines();
    refreshProducts();
    refreshAnswer();
}

/**
 * Adds a machine to the machines  
 * @returns {null}
 */
function addMachine() {
    const name = document.getElementById("machine-name").value;
    if (!name) {
        alert("you can't leave the name empty");
        return
    }
    const produce = document.getElementById("product-name").value;
    if (!produce) {
        alert("a machine must have a product");
        return
    }
    const needs = document.getElementById("machine-needs").querySelectorAll("input");
    const needsBuffer = [];
    for (let i in needs) {
        const need = needs[i].value;
        if (need) {
            needsBuffer.push(need);
        }
    }
    machines.push({
        name,
        produce,
        need: needsBuffer,
    });
    document.getElementById("machine-name").value = "";
    document.getElementById("product-name").value = "";
    for (let i in needs) {
        needs[i].value = "";
    }
    refresh();
}

refresh();