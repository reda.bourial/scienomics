/**
 * MachineMap is a map of the machines by product for faster lookups.
 */
class MachineMap {

    /**
     * @param {Array<Machine>} machines to index. 
     */
    constructor(machines) {
        this.machines = {};
        machines.map(m => this.machines[m.produce] = m);
    }

    /**
     * Get the machine making the product.
     * @param {string} product needed.
     * @returns {Machine} that makes the product. 
     */
    getMachine(product) {
        return this.machines[product];
    }

}

module.exports = { MachineMap }